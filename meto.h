#ifndef METO_H
#define METO_H

#include "noob.h"
#include <playerid.h>
#include <game.h>
#include <player.h>

namespace PXL2019
{
    class Meto : public Player
    {
    public:
     Meto( unsigned char instance = 0 )			: Player(instance)	{}

        virtual const char* getName( void ) const;
    protected:
        virtual int willYouRaise( unsigned int totalBet ); //MEMBER FUNCTION
        void speech( int debug );

        int WillYouRaise(unsigned int totalBet)
        {
   //ITS LOOKS LIKE THE VADER!!! ALGORITHM CUZ THE MOST COMING IS THREE OF A KIND AND TWO PAIR
            PokerRank myHandRank = getHand().getMyRank();
            PokerRank tableRank = getCommunityRank();
            if( myHandRank.getCategory() >= THREE_OF_A_KIND )
            {
                if( tableRank.getCategory() == myHandRank.getCategory() )
                {
                    return( 0 );
                }
                return(  totalBet - getBet() ); //raise as much as raised :-D troll
            }

         //THE MOST WINNING IS TWO PAIR (MY EXPERIENCE)
            else if( myHandRank.getCategory() >= TWO_PAIR )
            {
                if( tableRank.getCategory() == myHandRank.getCategory() )
                {
                    return( 0 );
                }
                return( getChips() );
            }
            else if( getTable()->getCommunityCards().empty() && ( getHand().getFirstCard()->getRank() == 1 || getHand().getSecondCard()->getRank() == 1 ) )
            {
                return( 1 );
            }
            else if( ( getBet() <= getGame()->getBlind() * 2 ) && ( totalBet > getGame()->getBlind() * 2 ) )
            {
                return( -1 );
            }
            return( 0 ); }
    private:
        int teller; //MEMBER VARIABLE
    };
}
#endif // METO_H

