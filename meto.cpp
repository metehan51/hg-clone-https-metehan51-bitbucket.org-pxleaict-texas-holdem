#include "meto.h"
#include "game.h"
#include "noob.h"
#include <stdlib.h>

namespace PXL2019
{
    const char* Meto::getName( void ) const
    {
        return "Meto the rebel";
    }

    int Meto::willYouRaise( unsigned int totalBet )
    {
        PokerRank tableRank = getCommunityRank();
        PokerRank myRank = getRank();

        if( getTable()->getCommunityCards().empty() && (
                    getHand().getFirstCard()->getSuit() == getHand().getSecondCard()->getSuit() ||
                    getHand().getFirstCard()->getRank() == 1 ||
                    getHand().getFirstCard()->getRank() >= 14 ||
                    getHand().getSecondCard()->getRank() == 1 ||
                    getHand().getSecondCard()->getRank() >= 14 ) )
        {
            int wantToRaise = ( getGame()->getBlind() * 4 * ( getInstance() + 1 ) ) - totalBet;
            if( wantToRaise >= 0 )
            {
                speech( 1 );
                return wantToRaise;
            }
            else if( wantToRaise < -40 && totalBet > 0.10 * getChips() )
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
        if( myRank.getCategory() > tableRank.getCategory() )
        {
            speech( 2 );
            return 10 * ( getInstance() + 1 ); //Raise
        }
        if( ( ! getTable()->getCommunityCards().empty() && myRank > tableRank ) ||
                tableRank.getCategory() >= FLUSH ||
                getBet() > getGame()->getBlind() * 5 ||
                totalBet < 0.40 * getChips() )
        {
            switch( getInstance() ) {
            case 0:
                return 0; //Call
                break;
            case 1:
            case 2:
                return 0; //Call
                break;
            case 3:
                speech( 3 );
                std::cout << ( myRank > tableRank ) << ( tableRank.getCategory() >= FLUSH ) << ( getBet() > getGame()->getBlind() * 5 ) << ( totalBet < 0.30 * getChips() ) << std::endl;
                return ( ( rand() % 100 ) < 75 )? 1 : 0; //75% Raise
                break;
            }
            return 0; //Call
        }
        else
        {
            if( getTable()->getCommunityCards().size() <= 4 )
            { //there is still one card to come
                int amountOfHeavyPlayers = 0;
                for( int i = 0 ; i < getGame()->getPlayers().size() ; i++ )
                {
                    if( getGame()->getPlayers().at( i )->getBet() >= 0.80 * getGame()->getHighestBet() )
                    {
                        amountOfHeavyPlayers++;
                    }
                }
                if( amountOfHeavyPlayers > 1 )
                {
                    return -1;
                }
                if( ( getGame()->getPlayers().size() - ( ( ( getGame()->getPlayerLocation( this ) - getGame()->getDealerLocation() ) + getGame()->getPlayers().size() ) % getGame()->getPlayers().size() ) ) <= 2 )
                {
                    return 0;
                }
            }
            return -1; //Fold
        }
    }

    void Meto::speech( int debug )
    {
        std::cout << "This bet is great ! We are great ! You are fake bet ! " << debug << std::endl;
    }

}
